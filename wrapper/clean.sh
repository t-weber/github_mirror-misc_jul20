#!/bin/bash

rm -fv expr_parser_wrap.cxx
rm -fv expr_parser.py
rm -rfv __pycache__
rm -vf *.so

rm -vf *.class
rm -f ExprParserD.java
rm -f expr_parser.java
rm -f expr_parserJNI.java
